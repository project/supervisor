/**
 * @file
 * JS behaviour for canonical process page.
 */
(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.supervisorProcessCanonical = {
    attach () {

      const [container] = once('supervisor-process-canonical', '[data-supervisor-selector="process-canonical-view"]');
      if (!container) {
        return;
      }
      const REFRESH_URL = container.dataset.supervisorRefreshUrl;
      const REFRESH_TIMEOUT = 1_000;

      const getElement = name => container.querySelector(`[data-supervisor-selector="${name}"]`);

      const startButton = getElement('start-action');
      const stopButton = getElement('stop-action');

      const itemEls = {
        name: getElement('name'),
        group: getElement('group'),
        pid: getElement('pid'),
        start: getElement('started'),
        stop: getElement('stopped'),
        uptime: getElement('uptime'),
        stateLabel: getElement('stateLabel'),
        exitStatus: getElement('exit_status'),
        stdoutLogfile: getElement('stdout_logfile'),
        stderrLogfile: getElement('stderr_logfile'),
      };

      const refreshHandler = async function () {
        const response = await fetch(REFRESH_URL);
        if (!response.ok) {
          throw new Error('Could not fetch process information.');
        }

        const values = await response.json();
        // eslint-disable-next-line no-restricted-syntax
        for (const [key, value] of Object.entries(values)) {
          if (itemEls[key] && itemEls[key].innerHTML !== value) {
            itemEls[key].innerHTML = value;
          }
        }
        // Highlight state label.
        // @see css/supervisor.css
        itemEls.stateLabel.dataset.supervisorProcessState = values.stateColor;

        // Claro admin theme style disabled buttons through 'is-disabled'
        // class.
        startButton.toggleAttribute('disabled', !values.isStartAllowed);
        startButton.classList.toggle('is-disabled', !values.isStartAllowed);
        stopButton.toggleAttribute('disabled', !values.isStopAllowed);
        stopButton.classList.toggle('is-disabled', !values.isStopAllowed);
      }

      const actionHandler = async function(event) {
        event.target.disabled = true;
        const ACTION_URL = event.target.dataset.supervisorActionUrl;
        const response = await fetch(ACTION_URL, {method: 'POST'});
        if (!response.ok) {
          throw new Error('Could not perform the operation.');
        }
      }

      startButton.addEventListener('click', actionHandler);
      stopButton.addEventListener('click', actionHandler);

      setInterval(refreshHandler, REFRESH_TIMEOUT)
    }
  };

} (window.Drupal, window.once));
