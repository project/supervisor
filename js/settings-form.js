/**
 * @file
 * JS behaviour for supervisor settings form.
 */
(function (Drupal, once) {
  'use strict';

  Drupal.behaviors.supervisorSettingsForm = {
    attach () {
      const [form] = once(
        'supervisor-settings',
        '[data-supervisor-selector=supervisor-settings]',
      );
      if (!form) {
        return;
      }

      const typeEl = form.querySelector('[data-supervisor-selector=connection-type]');
      const udsPathEl = form.querySelector('[data-supervisor-selector=uds-path]');
      const tcpUrlEl = form.querySelector('[data-supervisor-selector=tcp-url]');

      typeEl.addEventListener('change', () => {
        udsPathEl.toggleAttribute('hidden', typeEl.value === 'tcp');
        tcpUrlEl.toggleAttribute('hidden', typeEl.value === 'uds');
      });
    }
  };
}(window.Drupal, window.once));
