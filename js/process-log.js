/**
 * @file
 * JS behaviour for process log page.
 */
(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.supervisorProcessLog = {
    attach () {

      const [outputEl] = once('supervisor-process-canonical', '[data-supervisor-selector="process-log-output"]');
      if (!outputEl) {
        return;
      }

      const REFRESH_URL = outputEl.dataset.supervisorRefreshUrl;
      const refreshHandler = async function() {
        const response = await fetch(REFRESH_URL);
        if (!response.ok) {
          throw new Error('Could not fetch process log.');
        }
        outputEl.innerHTML = await response.text();
      }
      setInterval(refreshHandler, 1_000);
    }
  };

} (window.Drupal, window.once));
