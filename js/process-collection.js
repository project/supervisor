/**
 * @file
 * JS behaviour for process collection page.
 */
(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.supervisorProcessCollection = {
    async attach () {

      const [table] = once('supervisor-process-collection', '[data-supervisor-selector="process-collection"]');
      if (!table) {
        return;
      }

      const REFRESH_URL = table.dataset.supervisorRefreshUrl;
      const REFRESH_TIMEOUT = 1_000;

      const rows = table.querySelectorAll('[data-supervisor-process-row]');

      const refreshHandler = async function () {
        const response = await fetch(REFRESH_URL);
        if (!response.ok) {
          throw new Error('Could not fetch process information.');
        }
        const values = await response.json();

        const rowRefreshHandler = function(row) {
          const processName = row.dataset.supervisorProcessRow;
          const processValues = values.find(rowValues => processName === rowValues.name);
          if (!processValues) {
            // eslint-disable-next-line no-alert
            alert(`Could not refresh values for process "${processName}. Did you update supervisor configuration? Try to reload the page.`);
          }

          row.querySelectorAll('[data-supervisor-selector]').forEach(el => {
            const name = el.dataset.supervisorSelector;
            el.innerHTML = processValues[name];
            // Highlight state label.
            // @see css/supervisor.css
            if (name === 'stateLabel') {
              el.dataset.supervisorProcessState = processValues.stateColor;
            }
          })

          // Claro admin theme style disabled buttons through 'is-disabled'
          // class.
          const startButton = row.querySelector('[data-supervisor-selector="start-action"]');
          startButton.toggleAttribute('disabled', !processValues.isStartAllowed);
          startButton.classList.toggle('is-disabled', !processValues.isStartAllowed);

          const stopButton = row.querySelector('[data-supervisor-selector="stop-action"]');
          stopButton.toggleAttribute('disabled', !processValues.isStopAllowed);
          stopButton.classList.toggle('is-disabled', !processValues.isStopAllowed);
        }

        rows.forEach(rowRefreshHandler)
      }

      const actionHandler = async function (event) {
        const button = event.target;
        button.disabled = true;
        const URL = button.dataset.supervisorActionUrl;
        const response = await fetch(URL, {method: 'POST'});
        if (!response.ok) {
          throw new Error('Could not perform the operation.');
        }
      }
      table.querySelectorAll('[data-supervisor-action-url]').forEach(button => {
        button.addEventListener('click', actionHandler);
      })

      setInterval(refreshHandler, REFRESH_TIMEOUT)
    }
  };

} (window.Drupal, window.once));
