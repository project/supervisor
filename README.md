# Supervisor

A simple UI to control the programs running under Supervisor.

## Requirements

PHP 8.2
Drupal 10.1

## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
Navigate to /admin/config/services/supervisor/settings page and setup connection to Supervisor instance.
When using Unix domain socket make sure that web-server user has sufficient privileges to access the socket file.

## Usage
Admin UI: /admin/config/services/supervisor
CLI UI: `drush supervisor`

## Links

- Project page: https://www.drupal.org/project/supervisor
- Supervisor home page: http://supervisord.org
- Supervisor PHP library: https://github.com/supervisorphp/supervisor
