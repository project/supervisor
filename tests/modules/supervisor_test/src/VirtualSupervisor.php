<?php declare(strict_types = 1);

namespace Drupal\supervisor_test;

use Supervisor\Exception\Fault\BadNameException;
use Supervisor\ProcessInterface;
use Supervisor\ProcessStates;
use Supervisor\ServiceStates;
use Supervisor\SupervisorInterface;

/**
 * Stub implementation for supervisor handler.
 *
 * @method string getAPIVersion()
 * @method string getSupervisorVersion()
 * @method string getIdentification()
 * @method array getState()
 * @method int getPID()
 * @method string readLog(integer $offset, integer $limit)
 * @method bool clearLog()
 * @method bool shutdown()
 * @method bool restart()
 * @method array startAllProcesses(boolean $wait = TRUE)
 * @method array startProcessGroup(string $name, boolean $wait = TRUE)
 * @method array stopAllProcesses(boolean $wait = TRUE)
 * @method array stopProcessGroup(string $name, boolean $wait = TRUE)
 * @method bool sendProcessStdin(string $name, string $chars)
 * @method bool addProcessGroup(string $name)
 * @method bool removeProcessGroup(string $name)
 * @method string readProcessStdoutLog(string $name, integer $offset, integer $limit)
 * @method string readProcessStderrLog(string $name, integer $offset, integer $limit)
 * @method bool clearProcessLogs(string $name)
 * @method array clearAllProcessLogs()
 * @method array reloadConfig()
 * @method bool signalProcess(string $name, string $signal)
 * @method array signalProcessGroup(string $name, string $signal)
 * @method array signalAllProcesses(string $signal)
 */
final class VirtualSupervisor implements SupervisorInterface {

  /**
   * Constructs the object.
   */
  public function __construct(private ProcessInfoStorage $processInfoStorage) {}

  /**
   * {@inheritdoc}
   */
  public function call(string $namespace, string $method, array $arguments = []): mixed {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function __call(string $method, array $arguments): mixed {
    return match ($method) {
      'getIdentification' => 'Virtual Supervisor',
      'getSupervisorVersion', 'getAPIVersion' => '1.0.0',
      'getPID' => 100,
      default => NULL,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function getAllProcessInfo(): array {
    return $this->processInfoStorage->getAllProcessInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessInfo(string $processName): array {
    return $this->processInfoStorage->getProcessInfo($processName) ?? throw new BadNameException('BAD_NAME: ' . $processName);
  }

  /**
   * {@inheritdoc}
   */
  public function startProcess(string $name, bool $wait = TRUE): void {
    $process_info = $this->processInfoStorage->getProcessInfo($name);
    $process_info['start'] = \time();
    $process_info['pid'] = $process_info['start'] % 10_000;
    $process_info['state'] = ProcessStates::Running->value;
    $this->processInfoStorage->saveProcessInfo($process_info);
  }

  /**
   * {@inheritdoc}
   */
  public function stopProcess(string $name, bool $wait = TRUE): void {
    $process_info = $this->processInfoStorage->getProcessInfo($name);
    $process_info['stop'] = \time();
    $process_info['pid'] = NULL;
    $process_info['state'] = ProcessStates::Stopped->value;
    $this->processInfoStorage->saveProcessInfo($process_info);
  }

  /**
   * {@inheritdoc}
   */
  public function isConnected(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isRunning(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceState(): ServiceStates {
    return ServiceStates::Running;
  }

  /**
   * {@inheritdoc}
   */
  public function checkState(ServiceStates|int $checkState): bool {
    return $checkState === ServiceStates::Running;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllProcesses(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getProcess(string $name): ProcessInterface {
    // @todo Implement getProcess() method.
  }

  /**
   * {@inheritdoc}
   */
  public function tailProcessStdoutLog(string $name, int $offset, int $limit): array {
    $this->processInfoStorage->getProcessInfo($name);
    $log = \str_repeat("$name: stdout message\n", 100);
    $size = \strlen($log);
    return [$log, $size, $limit > $size];
  }

  /**
   * {@inheritdoc}
   */
  public function tailProcessStderrLog(string $name, int $offset, int $limit): array {
    $this->processInfoStorage->getProcessInfo($name);
    $log = \str_repeat("$name: stderr message\n", 100);
    $size = \strlen($log);
    return [$log, $size, $limit > $size];
  }

}
