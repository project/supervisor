<?php declare(strict_types = 1);

namespace Drupal\supervisor_test;

use Drupal\Core\State\StateInterface;
use Supervisor\Exception\Fault\BadNameException;

/**
 * Defines a storage for process information.
 */
final class ProcessInfoStorage {

  private const DATA_KEY = 'supervisor_test.all_process_info';

  /**
   * {@selfdoc}
   */
  public function __construct(private StateInterface $state) {}

  /**
   * {@selfdoc}
   */
  public function getAllProcessInfo(): array {
    $set_time = static fn (array $process_info): array => ['now' => \time()] + $process_info;
    return \array_map($set_time, $this->state->get(self::DATA_KEY));
  }

  /**
   * {@selfdoc}
   */
  public function saveAllProcessInfo(array $all_process_info): void {
    $this->state->set(self::DATA_KEY, $all_process_info);
  }

  /**
   * {@selfdoc}
   */
  public function getProcessInfo(string $process_name): array {
    $find_process_info = static fn (array $process_info): bool => $process_info['name'] === $process_name;
    $filtered_process_info = \array_filter($this->getAllProcessInfo(), $find_process_info);
    return \reset($filtered_process_info) ?: throw new BadNameException('BAD_NAME: ' . $process_name);
  }

  /**
   * {@selfdoc}
   */
  public function saveProcessInfo(array $process_info): void {
    $all_process_info = $this->getAllProcessInfo();
    foreach ($all_process_info as $delta => $stored_process_info) {
      if ($process_info['name'] === $stored_process_info['name']) {
        $all_process_info[$delta] = $process_info;
        $this->saveAllProcessInfo($all_process_info);
        return;
      }
    }
    throw new BadNameException('BAD_NAME: ' . $process_info['name']);
  }

}
