<?php declare(strict_types = 1);

namespace Drupal\supervisor_test;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Supervisor test module.
 */
final class SupervisorTestServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $container->getDefinition('supervisor.supervisor_client')
      ->setClass(VirtualSupervisor::class)
      ->addArgument(new Reference('supervisor_test.process_info_storage'))
      ->setFactory(NULL);
  }

}
