<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests date formatter.
 *
 * @group supervisor
 */
class DateTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor'];

  /**
   * Test callback.
   */
  public function testFormatter(): void {
    $this->installConfig('supervisor');

    $formatter = $this->container->get('supervisor.date_formatter');

    $date = $formatter->formatTimestamp(0);
    self::assertSame('', $date);

    $date = $formatter->formatTimestamp(1698225867);
    self::assertSame('2023-10-25 20:24:27', $date);

    $interval = $formatter->formatInterval(125);
    self::assertSame('2 min 5 sec', $interval);
  }

}
