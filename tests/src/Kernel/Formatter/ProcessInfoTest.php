<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\supervisor\Data\ProcessInfo;
use Supervisor\ProcessStates;

/**
 * Tests process info formatter.
 *
 * @group supervisor
 */
class ProcessInfoTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'supervisor_test',
    'supervisor',
  ];

  /**
   * Test callback.
   *
   * @dataProvider formatterData()
   */
  public function testFormatter(array $raw_process_info, ProcessInfo $expected_process_info): void {
    $this->installConfig('supervisor');
    $formatter = $this->container->get('supervisor.process_info_formatter');
    $actual_process_info = $formatter->format($raw_process_info);
    self::assertEquals($expected_process_info, $actual_process_info);
  }

  /**
   * {@selfdoc}
   */
  private static function formatterData(): \Generator {
    yield [
      [
        'name' => 'zippo',
        'group' => 'zippo',
        'state' => 20,
        'pid' => 100,
        'start' => 523_150_100,
        'stop' => 523_150_240,
        'now' => 523_150_500,
        'exitstatus' => 1,
        'stdout_logfile' => '/var/log/zippo-stdout.log',
        'stderr_logfile' => '/var/log/zippo-stderr.log',
      ],
      new ProcessInfo(
        name: 'zippo',
        group: 'zippo',
        pid: 100,
        start: '1986-07-31 09:28:20',
        stop: '1986-07-31 09:30:40',
        state: ProcessStates::Running,
        stateLabel: 'Running',
        stateColor: 'green',
        uptime: '6 min 40 sec',
        exitStatus: 1,
        isStartAllowed: FALSE,
        isStopAllowed: TRUE,
        stdoutLogfile: '/var/log/zippo-stdout.log',
        stderrLogfile: '/var/log/zippo-stderr.log',
      ),
    ];
    yield [
      [
        'name' => 'zippo',
        'group' => 'zippo',
        'state' => 40,
        'pid' => 100,
        'start' => 523_150_100,
        'stop' => 523_150_240,
        'now' => 523_150_500,
        'exitstatus' => 0,
        'stdout_logfile' => '/var/log/zippo-stdout.log',
        'stderr_logfile' => '/var/log/zippo-stderr.log',
      ],
      new ProcessInfo(
        name: 'zippo',
        group: 'zippo',
        pid: 100,
        start: '1986-07-31 09:28:20',
        stop: '1986-07-31 09:30:40',
        state: ProcessStates::Stopping,
        stateLabel: 'Stopping',
        stateColor: 'yellow',
        uptime: NULL,
        exitStatus: 0,
        isStartAllowed: FALSE,
        isStopAllowed: FALSE,
        stdoutLogfile: '/var/log/zippo-stdout.log',
        stderrLogfile: '/var/log/zippo-stderr.log',
      ),
    ];
    yield [
      [
        'name' => 'zippo',
        'group' => 'zippo',
        'state' => 0,
        'pid' => 100,
        'start' => 523_150_100,
        'stop' => 523_150_240,
        'now' => 523_150_500,
        'exitstatus' => 0,
        'stdout_logfile' => '/var/log/zippo-stdout.log',
        'stderr_logfile' => '/var/log/zippo-stderr.log',
      ],
      new ProcessInfo(
        name: 'zippo',
        group: 'zippo',
        pid: 100,
        start: '1986-07-31 09:28:20',
        stop: '1986-07-31 09:30:40',
        state: ProcessStates::Stopped,
        stateLabel: 'Stopped',
        stateColor: 'gray',
        uptime: NULL,
        exitStatus: 0,
        isStartAllowed: TRUE,
        isStopAllowed: FALSE,
        stdoutLogfile: '/var/log/zippo-stdout.log',
        stderrLogfile: '/var/log/zippo-stderr.log',
      ),
    ];
    yield [
      [
        'name' => 'zippo',
        'group' => 'zippo',
        'state' => 200,
        'pid' => 100,
        'start' => 523_150_100,
        'stop' => 523_150_240,
        'now' => 523_150_500,
        'exitstatus' => 0,
        'stdout_logfile' => '/var/log/zippo-stdout.log',
        'stderr_logfile' => '/var/log/zippo-stderr.log',
      ],
      new ProcessInfo(
        name: 'zippo',
        group: 'zippo',
        pid: 100,
        start: '1986-07-31 09:28:20',
        stop: '1986-07-31 09:30:40',
        state: ProcessStates::Fatal,
        stateLabel: 'Fatal',
        stateColor: 'red',
        uptime: NULL,
        exitStatus: 0,
        isStartAllowed: TRUE,
        isStopAllowed: FALSE,
        stdoutLogfile: '/var/log/zippo-stdout.log',
        stderrLogfile: '/var/log/zippo-stderr.log',
      ),
    ];
  }

}
