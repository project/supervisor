<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 *
 * @see \Drupal\Tests\supervisor\Functional\ProcessTest
 */
final class PermissionsTest extends BrowserTestBase {

  private const PATHS = [
    '/admin/config/services/supervisor',
    '/admin/config/services/supervisor/processes',
    '/admin/config/services/supervisor/processes/alpha',
    '/admin/config/services/supervisor/processes/alpha/refresh',
    '/admin/config/services/supervisor/processes/alpha/stdout',
    '/admin/config/services/supervisor/processes/alpha/stdout/tail',
    '/admin/config/services/supervisor/processes/alpha/stderr',
    '/admin/config/services/supervisor/processes/alpha/stderr/tail',
    '/admin/config/services/supervisor/settings',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor', 'supervisor_test'];

  /**
   * Test callback.
   */
  public function testAnonymous(): void {
    foreach (self::PATHS as $path) {
      $this->drupalGet($path);
      $this->assertSession()->statusCodeEquals(403);
    }
  }

  /**
   * Test callback.
   */
  public function testAuthenticated(): void {
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    foreach (self::PATHS as $path) {
      $this->drupalGet($path);
      $this->assertSession()->statusCodeEquals(403);
    }
  }

  /**
   * Test callback.
   */
  public function testPrivilegedUser(): void {
    $admin_user = $this->drupalCreateUser(['administer supervisor processes']);
    $this->drupalLogin($admin_user);
    foreach (self::PATHS as $path) {
      $this->drupalGet($path);
      $expected_code = $path === '/admin/config/services/supervisor/settings' ? 403 : 200;
      $this->assertSession()->statusCodeEquals($expected_code);
    }
  }

  /**
   * Test callback.
   */
  public function testAdmin(): void {
    $admin_user = $this->drupalCreateUser(['administer supervisor configuration']);
    $this->drupalLogin($admin_user);
    foreach (self::PATHS as $path) {
      $this->drupalGet($path);
      $expected_code = $path === '/admin/config/services/supervisor/settings' ? 200 : 403;
      $this->assertSession()->statusCodeEquals($expected_code);
    }
  }

}
