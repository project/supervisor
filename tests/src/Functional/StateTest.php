<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 */
final class StateTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor', 'supervisor_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(['administer supervisor processes']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test callback.
   */
  public function testStatePage(): void {
    $this->drupalGet('/admin/config/services/supervisor');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Supervisor"]');

    $this->assertItem('Identification', 'Virtual Supervisor');
    $this->assertItem('Version', '1.0.0');
    $this->assertItem('API version', '1.0.0');
    $this->assertItem('State', 'Running');
    $this->assertItem('PID', '100');
  }

  /**
   * {@selfdoc}
   */
  private function assertItem(string $label, string $value): void {
    $xpath = <<< XPATH
      //div[contains(@class, "field")]
      /div[@class = "field__label" and text() = "$label"]
      /following-sibling::div[@class = "field__item" and text() = "$value"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);
  }

}
