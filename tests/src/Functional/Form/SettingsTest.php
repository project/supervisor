<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 */
final class SettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser(['administer supervisor configuration']);
    $this->drupalLogin($user);
  }

  /**
   * Test callback.
   */
  public function testSettingsForm(): void {
    $this->drupalGet('/admin/config/services/supervisor/settings');

    $this->assertXpath('//h1[text() = "Settings"]');
    $this->assertFormMarkup();

    $edit = [
      'uds_path' => '/tmp/supervisor.sock',
      'date_format' => 'short',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertXpath('//div[@aria-label = "Status message" and contains(., "The configuration options have been saved.")]');
    $this->assertXpath('//input[@name = "uds_path" and @value = "/tmp/supervisor.sock"]');
    $this->assertXpath('//select[@name = "date_format" and ./option[@value = "short" and @selected]]');

    // Check that unprivileged users have no access to the form.
    $this->drupalLogout();
    $this->drupalGet('/admin/config/services/supervisor/settings');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * {@selfdoc}
   */
  private function assertFormMarkup(): void {
    // -- Connection type.
    $options_xpath = <<< 'XPATH'
      ./option[@value = "uds" and text() = "Unix domain socket"]
      /following-sibling::option[@value = "tcp" and text() = "TCP"]
      XPATH;
    $xpath = <<< XPATH
      //form/fieldset[./legend[contains(., "Connection")]]
      //label[text() = "Type"]
      /following-sibling::select[@name="connection_type" and $options_xpath]
      XPATH;
    $this->assertXpath($xpath);

    // -- User name.
    $username_description = 'Set in your local settings.php file as follows $settings[\'supervisor\'][\'username\'] = \'USERNAME\';';
    $xpath = <<< XPATH
      //form/fieldset[./legend[contains(., "Connection")]]
      //label[text() = "Username"]
      /following-sibling::input[@name="username" and @value ="" and @disabled="disabled"]
      /following-sibling::div[@class="description" and normalize-space(text()) = "$username_description"]
      XPATH;
    $this->assertXpath($xpath);

    // -- Password.
    $username_description = 'Set in your local settings.php file as follows $settings[\'supervisor\'][\'password\'] = \'PASSWORD\';';
    $xpath = <<< XPATH
      //form/fieldset[./legend[contains(., "Connection")]]
      //label[text() = "Password"]
      /following-sibling::input[@name="password" and @value ="" and @disabled="disabled"]
      /following-sibling::div[@class="description" and normalize-space(text()) = "$username_description"]
      XPATH;
    $this->assertXpath($xpath);

    // -- Unix socket path.
    $uds_path_description = 'The path can be absolute or relative to Drupal root directory.';
    $xpath = <<< XPATH
      //form/fieldset[./legend[contains(., "Connection")]]
      //label[text() = "Unix socket path"]
      /following-sibling::input[@name="uds_path" and @value ="/var/run/supervisor.sock"]
      /following-sibling::div[@class="description" and normalize-space(text()) = "$uds_path_description"]
      XPATH;
    $this->assertXpath($xpath);

    // -- TCP URL.
    $xpath = <<< 'XPATH'
      //form/fieldset[./legend[contains(., "Connection")]]
      //label[text() = "TCP URL"]
      /following-sibling::input[@name="tcp_url" and @value ="http://127.0.0.1:9001/rpc2"]
      XPATH;
    $this->assertXpath($xpath);

    // -- Date format.
    $xpath = <<< 'XPATH'
      //form
      //label[text() = "Date format"]
      /following-sibling::select[@name="date_format" and ./option[@value = "supervisor_iso_8601" and @selected]]
      XPATH;
    $this->assertXpath($xpath);

    // -- Submit.
    $xpath = <<< 'XPATH'
      //form//div[contains(@class, "form-actions")]
      /input[@value = "Save configuration"]
      XPATH;
    $this->assertXpath($xpath);
  }

  /**
   * {@selfdoc}
   */
  private function assertXpath(string $xpath): void {
    $this->assertSession()->elementExists('xpath', $xpath);
  }

}
