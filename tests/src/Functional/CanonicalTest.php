<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 */
final class CanonicalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor', 'supervisor_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(['administer supervisor processes']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test callback.
   */
  public function testCanonicalPage(): void {
    $this->drupalGet('/admin/config/services/supervisor/processes/alpha');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "alpha"]');

    $this->assertItem('Name', 'alpha');
    $this->assertItem('Group', 'alpha');
    $this->assertItem('PID', '');
    $this->assertItem('Started', '');
    $this->assertItem('Stopped', '');
    $this->assertItem('State', 'Stopped');
    $this->assertItem('Uptime', '');
    $this->assertItem('Exit status', '');
    $this->assertItem('StdOut log file', '/var/log/alpha-stdout---supervisor-wx31g93l.log');
    $this->assertItem('StdErr log file', '/var/log/alpha-stderr---supervisor-xusgxo0w.log');

    $this->assertSession()->elementExists('xpath', '//input[@value = "Start" and not(@disabled)]');
    $this->assertSession()->elementExists('xpath', '//input[@value = "Stop" and @disabled]');

    // Start the process and check if the page reflects its state.
    $supervisor = $this->container->get('supervisor.supervisor_client');
    $supervisor->startProcess('alpha');
    $this->drupalGet('/admin/config/services/supervisor/processes/alpha');

    $process_info = $supervisor->getProcessInfo('alpha');

    $this->assertItem('Name', 'alpha');
    $this->assertItem('Group', 'alpha');
    $this->assertItem('PID', $process_info['pid']);

    $date_formatter = $this->container->get('date.formatter');
    $start = $process_info['start'] ? $date_formatter->format($process_info['start'], 'custom', 'Y-m-d H:i:s') : NULL;
    $stop = $process_info['stop'] ? $date_formatter->format($process_info['stop'], 'custom', 'Y-m-d H:i:s') : NULL;
    $this->assertItem('Started', $start);
    $this->assertItem('Stopped', $stop);
    $this->assertItem('State', 'Running');

    $td = $this->assertSession()->elementExists('xpath', '//td[@data-supervisor-selector="uptime"]');
    self::assertMatchesRegularExpression('/\d sec/', $td->getText());

    $this->assertItem('Exit status', '');
    $this->assertItem('StdOut log file', '/var/log/alpha-stdout---supervisor-wx31g93l.log');
    $this->assertItem('StdErr log file', '/var/log/alpha-stderr---supervisor-xusgxo0w.log');

    $this->assertSession()->elementExists('xpath', '//input[@value = "Start" and @disabled]');
    $this->assertSession()->elementExists('xpath', '//input[@value = "Stop" and not(@disabled)]');
  }

  /**
   * {@selfdoc}
   */
  private function assertItem(string $label, int|string|null $value): void {
    $xpath = <<< XPATH
      //tr
      /th[text() = "$label"]
      /following-sibling::td[. = "$value"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);
  }

}
