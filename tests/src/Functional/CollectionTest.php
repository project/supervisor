<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 */
final class CollectionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor', 'supervisor_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(['administer supervisor processes']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test callback.
   */
  public function testCollectionPage(): void {
    $this->drupalGet('/admin/config/services/supervisor/processes');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Processes"]');

    $this->assertItem(
      name: 'alpha',
      group: 'alpha',
      pid: NULL,
      start: NULL,
      stop: NULL,
      stateLabel: 'Stopped',
      uptime: NULL,
      isStartAllowed: TRUE,
      isStopAllowed: FALSE,
    );

    $this->assertItem(
      name: 'beta',
      group: 'beta',
      pid: NULL,
      start: NULL,
      stop: NULL,
      stateLabel: 'Stopped',
      uptime: NULL,
      isStartAllowed: TRUE,
      isStopAllowed: FALSE,
    );

    $this->assertItem(
      name: 'gamma',
      group: 'gamma',
      pid: NULL,
      start: NULL,
      stop: NULL,
      stateLabel: 'Stopped',
      uptime: NULL,
      isStartAllowed: TRUE,
      isStopAllowed: FALSE,
    );

    // Update some processes and check if the page reflects their state.
    $supervisor = $this->container->get('supervisor.supervisor_client');
    $supervisor->startProcess('alpha');
    $supervisor->stopProcess('alpha');
    $supervisor->startProcess('beta');
    $this->drupalGet('/admin/config/services/supervisor/processes');

    $all_process_info = $this->container->get('state')->get('supervisor_test.all_process_info');

    $this->assertItem(
      name: 'alpha',
      group: 'alpha',
      pid: $all_process_info[0]['pid'],
      start: $all_process_info[0]['start'],
      stop: $all_process_info[0]['stop'],
      stateLabel: 'Stopped',
      uptime: NULL,
      isStartAllowed: TRUE,
      isStopAllowed: FALSE,
    );

    $this->assertItem(
      name: 'beta',
      group: 'beta',
      pid: $all_process_info[1]['pid'],
      start: $all_process_info[1]['start'],
      stop: NULL,
      stateLabel: 'Running',
      uptime: '0 sec',
      isStartAllowed: FALSE,
      isStopAllowed: TRUE,
    );

    $this->assertItem(
      name: 'gamma',
      group: 'gamma',
      pid: NULL,
      start: NULL,
      stop: NULL,
      stateLabel: 'Stopped',
      uptime: NULL,
      isStartAllowed: TRUE,
      isStopAllowed: FALSE,
    );
  }

  /**
   * {@selfdoc}
   */
  private function assertItem(
    string $name,
    string $group,
    ?int $pid,
    ?int $start,
    ?int $stop,
    string $stateLabel,
    ?string $uptime,
    bool $isStartAllowed,
    bool $isStopAllowed,
  ): void {
    $date_formatter = $this->container->get('date.formatter');
    $start = $start ? $date_formatter->format($start, 'custom', 'Y-m-d H:i:s') : NULL;
    $stop = $stop ? $date_formatter->format($stop, 'custom', 'Y-m-d H:i:s') : NULL;

    $start_disabled = $isStartAllowed ? 'not(@disabled)' : '@disabled';
    $stop_disabled = $isStopAllowed ? 'not(@disabled)' : '@disabled';

    $xpath = <<< XPATH
      //tr[@data-supervisor-process-row = "$name"]
      /td[1][./a[text() = "$name"]]
      /following-sibling::td[@data-supervisor-selector = "pid" and . = "$pid"]
      /following-sibling::td[@data-supervisor-selector = "start" and . = "$start"]
      /following-sibling::td[@data-supervisor-selector = "stop" and . = "$stop"]
      /following-sibling::td[span[@data-supervisor-selector = "stateLabel" and text() = "$stateLabel"]]
      /following-sibling::td[@data-supervisor-selector = "uptime" and . = "$uptime"]
      /following-sibling::td/div
      /input[@value = "Start" and $start_disabled]
      /following-sibling::input[@value = "Stop" and $stop_disabled]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);
  }

}
