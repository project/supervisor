<?php declare(strict_types = 1);

namespace Drupal\Tests\supervisor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * {@selfdoc}
 *
 * @group supervisor
 *
 * @see \Drupal\Tests\supervisor\Functional\PermissionsTest
 */
final class ProcessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['supervisor', 'supervisor_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(['administer supervisor processes']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test callback.
   *
   * @dataProvider processAccessData()
   */
  public function testProcessAccess(string $path, int $status_code): void {
    $this->drupalGet($path);
    $this->assertSession()->statusCodeEquals($status_code);
  }

  /**
   * {@selfdoc}
   */
  private static function processAccessData(): array {
    return [
      ['/admin/config/services/supervisor/processes/alpha', 200],
      ['/admin/config/services/supervisor/processes/wrong', 404],
      ['/admin/config/services/supervisor/processes/alpha/refresh', 200],
      ['/admin/config/services/supervisor/processes/wrong_process/refresh', 404],
      ['/admin/config/services/supervisor/processes/alpha/stdout', 200],
      ['/admin/config/services/supervisor/processes/wrong_process/stdout', 404],
      ['/admin/config/services/supervisor/processes/alpha/stderr', 200],
      ['/admin/config/services/supervisor/processes/wrong_process/stderr', 404],
      ['/admin/config/services/supervisor/processes/alpha/wrong_stream', 404],
      ['/admin/config/services/supervisor/processes/wrong_process/wrong_stream', 404],
      ['/admin/config/services/supervisor/processes/alpha/operation/start', 405],
      ['/admin/config/services/supervisor/processes/alpha/operation/stop', 405],
    ];
  }

}
