<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller\Process;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\supervisor\Formatter;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Returns responses for Supervisor routes.
 */
final readonly class CollectionRefresh implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private SupervisorInterface $supervisor,
    private Formatter\ProcessInfo $formatter,
  ) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('supervisor.supervisor_client'),
      $container->get('supervisor.process_info_formatter'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): JsonResponse {
    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      throw new HttpException(Response::HTTP_BAD_GATEWAY);
    }
    $all_process_info = \array_map(
      [$this->formatter, 'format'],
      $this->supervisor->getAllProcessInfo(),
    );
    return new JsonResponse($all_process_info);
  }

}
