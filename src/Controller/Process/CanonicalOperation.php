<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller\Process;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use fXmlRpc\Exception\FaultException;
use Supervisor\Exception\SupervisorException;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Supervisor routes.
 */
final readonly class CanonicalOperation implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private SupervisorInterface $supervisor,
  ) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('supervisor.supervisor_client'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(string $process_name, string $operation): JsonResponse {
    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      throw new HttpException(Response::HTTP_BAD_GATEWAY);
    }

    // @todo Protect the URL with tokens.
    return match ($operation) {
      'start' => $this->startProcess($process_name),
      'stop' => $this->stopProcess($process_name),
      default => throw new NotFoundHttpException(),
    };
  }

  /**
   * {@selfdoc}
   */
  private function startProcess(string $process_name): JsonResponse {
    try {
      $this->supervisor->startProcess($process_name, FALSE);
    }
    catch (FaultException | SupervisorException $exception) {
      $result = ['error' => $exception->getMessage()];
      return new JsonResponse($result, status: Response::HTTP_BAD_REQUEST);
    }
    return new JsonResponse(status: Response::HTTP_NO_CONTENT);
  }

  /**
   * {@selfdoc}
   */
  private function stopProcess(string $process_name): JsonResponse {
    try {
      $this->supervisor->stopProcess($process_name, FALSE);
    }
    catch (FaultException | SupervisorException $exception) {
      $result = ['error' => $exception->getMessage()];
      return new JsonResponse($result, status: Response::HTTP_BAD_REQUEST);
    }
    return new JsonResponse(status: Response::HTTP_NO_CONTENT);
  }

}
