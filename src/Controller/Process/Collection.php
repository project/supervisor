<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller\Process;

use Drupal\Component\Render\FormattableMarkup as FM;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Url;
use Drupal\supervisor\Formatter;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays a process collection page.
 */
final readonly class Collection implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private SupervisorInterface $supervisor,
    private Formatter\ProcessInfo $formatter,
  ) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('supervisor.supervisor_client'),
      $container->get('supervisor.process_info_formatter'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      return ['#markup' => new TM('Could not connect to Supervisor.')];
    }

    $header = [
      ['data' => new TM('Name')],
      ['data' => new TM('Group')],
      ['data' => new TM('PID'), 'class' => 'supervisor-collection-header-pid'],
      ['data' => new TM('Started')],
      ['data' => new TM('Stopped')],
      ['data' => new TM('State')],
      ['data' => new TM('Uptime'), 'class' => 'supervisor-collection-header-uptime'],
      ['data' => new TM('Operations'), 'class' => 'supervisor-collection-header-operations'],
    ];

    $refresh_url = Url::fromRoute('supervisor.process_collection.refresh');

    $processes_info = $this->supervisor->getAllProcessInfo();
    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => \array_map([$this, 'buildProcessRow'], $processes_info),
      '#empty' => new TM('No processes were found.'),
      '#attributes' => [
        'data-supervisor-selector' => 'process-collection',
        'data-supervisor-refresh-url' => $refresh_url->toString(),
      ],
      '#attached' => ['library' => ['supervisor/supervisor']],
    ];

    return $build;
  }

  /**
   * {@selfdoc}
   */
  private function buildProcessRow(array $raw_process_info): array {

    $process_info = $this->formatter->format($raw_process_info);

    $route_parameters = ['process_name' => $process_info->group . ':' . $process_info->name];
    $link = Link::createFromRoute(
      $process_info->name, 'supervisor.process_canonical', $route_parameters,
    );
    $start_url = Url::fromRoute(
      'supervisor.process_canonical.operation', $route_parameters + ['operation' => 'start'],
    );
    $stop_url = Url::fromRoute(
      'supervisor.process_canonical.operation', $route_parameters + ['operation' => 'stop'],
    );

    $actions = [
      '#type' => 'actions',
      'start' => [
        '#type' => 'button',
        '#value' => new TM('Start'),
        '#button_type' => 'small',
        '#attributes' => [
          'disabled' => !$process_info->isStartAllowed,
          'data-supervisor-selector' => 'start-action',
          'data-supervisor-action-url' => $start_url->toString(),
        ],
      ],
      'stop' => [
        '#type' => 'button',
        '#value' => new TM('Stop'),
        '#button_type' => 'small',
        '#attributes' => [
          'disabled' => !$process_info->isStopAllowed,
          'data-supervisor-selector' => 'stop-action',
          'data-supervisor-action-url' => $stop_url->toString(),
        ],
      ],
    ];

    $markup = '<span data-supervisor-selector="stateLabel" data-supervisor-process-state="@color">@label</span>';
    $arguments = [
      '@color' => $process_info->stateColor,
      '@label' => $process_info->stateLabel,
    ];
    $state_formatted = new FM($markup, $arguments);

    $row_data = [
      $link,
      $process_info->group,
      ['data' => $process_info->pid ?: '', 'data-supervisor-selector' => 'pid'],
      ['data' => $process_info->start, 'data-supervisor-selector' => 'start'],
      ['data' => $process_info->stop, 'data-supervisor-selector' => 'stop'],
      ['data' => $state_formatted],
      ['data' => $process_info->uptime, 'data-supervisor-selector' => 'uptime'],
      ['data' => $actions],
    ];
    return [
      'data' => $row_data,
      'data-supervisor-process-row' => $process_info->name,
    ];
  }

}
