<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller\Process;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Url;
use Drupal\supervisor\Data\StreamType;
use Supervisor\Exception\Fault\BadNameException;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Supervisor routes.
 */
final readonly class CanonicalLog implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private SupervisorInterface $supervisor) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('supervisor.supervisor_client'));
  }

  /**
   * Builds the response.
   */
  public function page(string $process_name, string $type): array {

    $stream_type = StreamType::tryFrom($type);
    if (!$stream_type) {
      throw new NotFoundHttpException();
    }

    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      return ['#markup' => new TM('Could not connect to Supervisor.')];
    }

    try {
      $log_tail = $this->getLogTail($process_name, $stream_type);
    }
    catch (BadNameException) {
      throw new NotFoundHttpException('Process does not exist.');
    }

    $refresh_url = Url::fromRoute(
      'supervisor.process_canonical.log_tail',
      ['process_name' => $process_name, 'type' => $stream_type->value],
    );

    return [
      '#type' => 'html_tag',
      '#tag' => 'output',
      '#value' => $log_tail,
      '#attributes' => [
        'data-supervisor-selector' => 'process-log-output',
        'data-supervisor-refresh-url' => $refresh_url->toString(),
        'class' => ['supervisor-process-log-output'],
      ],
      '#attached' => ['library' => 'supervisor/supervisor'],
    ];
  }

  /**
   * Builds the response.
   */
  public function tail(string $process_name, $type): Response {

    $stream_type = StreamType::tryFrom($type);
    if (!$stream_type) {
      throw new NotFoundHttpException();
    }

    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      throw new HttpException(Response::HTTP_BAD_GATEWAY);
    }

    try {
      $log = $this->getLogTail($process_name, $stream_type);
    }
    catch (\Exception) {
      throw new HttpException(Response::HTTP_BAD_GATEWAY);
    }

    $headers = ['Content-Type' => 'text/plain;charset=UTF-8'];
    return new Response($log, headers: $headers);
  }

  /**
   * {@selfdoc}
   */
  private function getLogTail(string $process_name, StreamType $streamType): string {
    $method = match ($streamType) {
      StreamType::STDOUT => 'tailProcessStdoutLog',
      StreamType::STDERR => 'tailProcessStderrLog',
    };
    $log = $this->supervisor->$method($process_name, 0, 10_000)[0];

    $lines = explode("\n", $log);
    $lines = array_map('trim', $lines);
    return \implode("\n", $lines);
  }

}
