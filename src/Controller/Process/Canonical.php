<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller\Process;

use Drupal\Component\Render\FormattableMarkup as FM;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Url;
use Drupal\supervisor\Formatter;
use Supervisor\Exception\Fault\BadNameException;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * {@selfdoc}
 */
final readonly class Canonical implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private SupervisorInterface $supervisor,
    private Formatter\ProcessInfo $formatter,
  ) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('supervisor.supervisor_client'),
      $container->get('supervisor.process_info_formatter'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(string $process_name): array {
    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      return ['#markup' => new TM('Could not connect to Supervisor.')];
    }

    try {
      $raw_process_info = $this->supervisor->getProcessInfo($process_name);
    }
    catch (BadNameException) {
      throw new NotFoundHttpException('Process does not exist.');
    }

    $process_info = $this->formatter->format($raw_process_info);

    $refresh_url = Url::fromRoute(
      'supervisor.process_canonical.refresh', ['process_name' => $process_name],
    );

    $container = [
      '#type' => 'container',
      '#attributes' => [
        'data-supervisor-selector' => 'process-canonical-view',
        'data-supervisor-refresh-url' => $refresh_url->toString(),
      ],
      '#attached' => ['library' => ['supervisor/supervisor']],
    ];
    $build = &$container;

    $build['#title'] = $process_info->name;

    $rows[] = self::buildRow(
      label: new TM('Name'),
      key: 'name',
      value: $process_info->name,
    );

    $rows[] = self::buildRow(
      label: new TM('Group'),
      key: 'group',
      value: $process_info->group,
    );

    $rows[] = self::buildRow(
      label: new TM('PID'),
      key: 'pid',
      value: $process_info->pid,
    );

    $rows[] = self::buildRow(
      label: new TM('Started'),
      key: 'started',
      value: $process_info->start,
    );

    $rows[] = self::buildRow(
      label: new TM('Stopped'),
      key: 'stopped',
      value: $process_info->stop,
    );

    $state_formatted = new FM(
      '<span data-supervisor-selector="stateLabel" data-supervisor-process-state="@color">@label</span>',
      [
        '@color' => $process_info->stateColor,
        '@label' => $process_info->stateLabel,
      ],
    );

    $rows[] = self::buildRow(
      label: new TM('State'),
      key: NULL,
      value: $state_formatted,
    );

    $rows[] = self::buildRow(
      label: new TM('Uptime'),
      key: 'uptime',
      value: $process_info->uptime,
    );

    $rows[] = self::buildRow(
      label: new TM('Exit status'),
      key: 'exit_status',
      value: $process_info->exitStatus,
    );

    $rows[] = self::buildRow(
      label: new TM('StdOut log file'),
      key: 'stdout_logfile',
      value: $process_info->stdoutLogfile,
    );

    $rows[] = self::buildRow(
      label: new TM('StdErr log file'),
      key: 'stderr_logfile',
      value: $process_info->stderrLogfile,
    );

    $build['data'] = [
      '#type' => 'table',
      '#rows' => $rows,
    ];

    $build['actions'] = ['#type' => 'action'];

    $start_url = Url::fromRoute(
      'supervisor.process_canonical.operation',
      ['process_name' => $process_name, 'operation' => 'start'],
    );
    $build['actions']['start'] = [
      '#type' => 'button',
      '#value' => new TM('Start'),
      '#attributes' => [
        'disabled' => !$process_info->isStartAllowed,
        'data-supervisor-selector' => 'start-action',
        'data-supervisor-action-url' => $start_url->toString(),
      ],
    ];

    $stop_url = Url::fromRoute(
      'supervisor.process_canonical.operation',
      ['process_name' => $process_name, 'operation' => 'stop'],
    );
    $build['actions']['stop'] = [
      '#type' => 'submit',
      '#value' => new TM('Stop'),
      '#attributes' => [
        'disabled' => !$process_info->isStopAllowed,
        'data-supervisor-selector' => 'stop-action',
        'data-supervisor-action-url' => $stop_url->toString(),
      ],
    ];

    return $build;
  }

  /**
   * {@selfdoc}
   */
  private static function buildRow(TM $label, ?string $key, FM|string|int|null $value): array {
    $th = ['data' => $label, 'header' => TRUE];
    $td = ['data' => $value];
    if ($key) {
      $td['data-supervisor-selector'] = $key;
    }
    return ['data' => [$th, $td]];
  }

}
