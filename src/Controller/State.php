<?php declare(strict_types = 1);

namespace Drupal\supervisor\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Supervisor\ServiceStates;
use Supervisor\SupervisorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Supervisor routes.
 */
final readonly class State implements ContainerInjectionInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private SupervisorInterface $supervisor) {}

  /**
   * {@selfdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('supervisor.supervisor_client'));
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      return [
        '#markup' => new TM('Could not connect to Supervisor.<br/>@message', ['@message' => $this->getConnectionError()]),
      ];
    }

    $identification = $this->supervisor->getIdentification();
    $build['identification'] = [
      '#theme' => 'supervisor_field',
      '#label' => new TM('Identification'),
      '#items' => [$identification],
    ];

    $api_version = $this->supervisor->getSupervisorVersion();
    $build['version'] = [
      '#theme' => 'supervisor_field',
      '#label' => new TM('Version'),
      '#items' => [$api_version],
    ];

    $api_version = $this->supervisor->getAPIVersion();
    $build['api_version'] = [
      '#theme' => 'supervisor_field',
      '#label' => new TM('API version'),
      '#items' => [$api_version],
    ];

    $state = $this->supervisor->getServiceState();
    $state_label = match ($state) {
      ServiceStates::Shutdown => new TM('Shutdown'),
      ServiceStates::Restarting => new TM('Restarting'),
      ServiceStates::Running => new TM('Running'),
      ServiceStates::Fatal => new TM('Fatal'),
    };
    $build['state'] = [
      '#theme' => 'supervisor_field',
      '#label' => new TM('State'),
      '#items' => [$state_label],
    ];

    $pid = $this->supervisor->getPID();
    $build['pid'] = [
      '#theme' => 'supervisor_field',
      '#label' => new TM('PID'),
      '#items' => [$pid],
    ];

    return $build;
  }

  /**
   * {@selfdoc}
   */
  private function getConnectionError(): string {
    try {
      /* @noinspection PhpExpressionResultUnusedInspection */
      $this->supervisor->getServiceState();
    }
    catch (\Throwable $throwable) {
      return $throwable->getMessage();
    }
    return '';
  }

}
