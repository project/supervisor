<?php declare(strict_types = 1);

namespace Drupal\supervisor\Factory;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;
use fXmlRpc\Client;
use fXmlRpc\ClientInterface;
use fXmlRpc\Transport\PsrTransport;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\HttpFactory;

/**
 * Creates an RPC client.
 */
final readonly class RpcClient {

  /**
   * Constructs the object.
   */
  public function __construct(
    private ConfigFactoryInterface $configFactory,
    private string $drupalRoot,
  ) {}

  /**
   * {@selfdoc}
   */
  public function get(): ClientInterface {
    $path = $this->configFactory->get('supervisor.settings')->get('uds_path');
    if (!\str_starts_with($path, '/')) {
      $path = $this->drupalRoot . '/' . $path;
    }

    $config['curl'][\CURLOPT_UNIX_SOCKET_PATH] = $path;
    $username = Settings::get('supervisor')['username'] ?? NULL;
    $password = Settings::get('supervisor')['password'] ?? NULL;
    if ($username || $password) {
      $config['auth'] = [$username, $password];
    }
    $transport = new PsrTransport(new HttpFactory(), new GuzzleClient($config));
    // @todo Make the RPC URI configurable.
    return new Client('http://localhost/RPC2', $transport);
  }

}
