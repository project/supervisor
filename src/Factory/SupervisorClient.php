<?php declare(strict_types = 1);

namespace Drupal\supervisor\Factory;

use fXmlRpc\Client;
use Supervisor\Supervisor;

/**
 * Creates a supervisor client.
 */
final readonly class SupervisorClient {

  /**
   * {@selfdoc}
   */
  public function __construct(private Client $rpcClient) {}

  /**
   * {@selfdoc}
   */
  public function get(): Supervisor {
    return new Supervisor($this->rpcClient);
  }

}
