<?php declare(strict_types = 1);

namespace Drupal\supervisor\Command;

use Supervisor\Exception\Fault\BadNameException;
use Supervisor\Exception\Fault\NotRunningException;
use Supervisor\Exception\SupervisorException;
use Supervisor\SupervisorInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
  name: 'supervisor:process:stop',
  description: 'Stops process',
  aliases: ['sp-stop'],
)]
final class ProcessStop extends Command {

  /**
   * {@inheritdoc}
   */
  public function __construct(private readonly SupervisorInterface $supervisor) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->addArgument(
        name: 'process',
        mode: InputArgument::IS_ARRAY | InputArgument::REQUIRED,
        description: 'Process name to filter results.',
      )
      ->addOption(
        name: 'wait',
        description: 'Wait for processes to be fully stopped.',
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    $io = new SymfonyStyle($input, $output);

    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      $io->getErrorStyle()->error('Could not connect to supervisor instance.');
      return self::FAILURE;
    }

    $wait = $input->getOption('wait');

    $failure = FALSE;
    foreach ($input->getArgument('process') as $process_name) {
      if ($io->isVerbose()) {
        $io->writeln(\sprintf('Stopping process "%s".', $process_name));
      }
      try {
        $this->supervisor->stopProcess($process_name, $wait);
        $io->writeln(\sprintf('🏁 Stopped "%s" process.', $process_name));
      }
      catch (SupervisorException $exception) {
        $message = match($exception::class) {
          BadNameException::class => \sprintf('Could not find process "%s".', $process_name),
          NotRunningException::class => \sprintf('Process "%s" is not running.', $process_name),
          default => $exception->getMessage(),
        };
        $io->getErrorStyle()->error($message);
        $failure = TRUE;
      }
    }

    return $failure ? self::FAILURE : self::SUCCESS;
  }

}
