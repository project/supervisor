<?php declare(strict_types = 1);

namespace Drupal\supervisor\Command;

use Drupal\supervisor\Formatter;
use Supervisor\ProcessStates;
use Supervisor\SupervisorInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
  name: 'supervisor:process:status',
  description: 'Checks process status',
  aliases: ['sp-status'],
)]
final class ProcessStatus extends Command {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly SupervisorInterface $supervisor,
    private readonly Formatter\ProcessInfo $formatter,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->addArgument(
        name: 'process',
        mode: InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
        description: 'Process name to filter results.',
      )
      ->addOption(
        name: 'state',
        mode: InputOption::VALUE_OPTIONAL,
        description: 'Process state to filter results.',
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    $io = new SymfonyStyle($input, $output);

    if (!$this->supervisor->isConnected() || !$this->supervisor->isRunning()) {
      $io->getErrorStyle()->error('Could not connect to supervisor instance.');
      return self::FAILURE;
    }

    $state = NULL;
    if ($state_encoded = $input->getOption('state')) {
      $state = match (\strtolower($state_encoded)) {
        'stopped' => ProcessStates::Stopped,
        'starting' => ProcessStates::Starting,
        'running' => ProcessStates::Running,
        'backoff' => ProcessStates::Backoff,
        'stopping' => ProcessStates::Stopping,
        'exited' => ProcessStates::Exited,
        'fatal' => ProcessStates::Fatal,
        'unknown' => ProcessStates::Unknown,
        default => NULL,
      };
      if (!$state) {
        $io->getErrorStyle()->error(\sprintf('Unsupported process state "%s".', $state_encoded));
        return self::FAILURE;
      }
    }

    $process_names = $input->getArgument('process');

    $table = $io->createTable();
    $table->setHeaders(['Name', 'Group', 'PID', 'Started', 'Stopped', 'State', 'Uptime']);

    foreach ($this->supervisor->getAllProcessInfo() as $raw_process_info) {
      $process_info = $this->formatter->format($raw_process_info);
      if ($state && $process_info->state !== $state) {
        continue;
      }
      if (\count($process_names) > 0 && !\in_array($process_info->name, $process_names)) {
        continue;
      }
      $row = [
        $process_info->name,
        $process_info->group,
        $process_info->pid,
        $process_info->start,
        $process_info->stop,
        $process_info->stateLabel,
        $process_info->uptime,
      ];
      $table->addRow($row);
    }

    $table->render();
    return self::SUCCESS;
  }

}
