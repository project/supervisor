<?php declare(strict_types = 1);

namespace Drupal\supervisor\Formatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Date formatter for Supervisor UI.
 */
final readonly class Date {

  /**
   * Constructs the object.
   */
  public function __construct(
    private DateFormatterInterface $dateFormatter,
    private ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@selfdoc}
   */
  public function formatTimestamp(int $timestamp): string {
    if ($timestamp === 0) {
      return '';
    }
    return $this->dateFormatter->format(
      $timestamp,
      $this->configFactory->get('supervisor.settings')->get('date_format'),
    );
  }

  /**
   * {@selfdoc}
   */
  public function formatInterval(int $interval): string {
    /* @noinspection PhpCastIsUnnecessaryInspection */
    return (string) $this->dateFormatter->formatInterval($interval);
  }

}
