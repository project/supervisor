<?php declare(strict_types = 1);

namespace Drupal\supervisor\Formatter;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\supervisor\Data;
use Drupal\supervisor\Formatter;
use Supervisor\ProcessStates;

/**
 * Formats process information.
 */
final readonly class ProcessInfo {

  /**
   * {@selfdoc}
   */
  public function __construct(private Formatter\Date $dateFormatter) {}

  /**
   * {@selfdoc}
   */
  public function format(array $raw_process_info): Data\ProcessInfo {
    $state = ProcessStates::from($raw_process_info['state']);

    $pid = $raw_process_info['pid'] === 0 ?
      NULL : $raw_process_info['pid'];

    $start = $raw_process_info['start'] === 0 ?
      NULL : $this->dateFormatter->formatTimestamp($raw_process_info['start']);

    $stop = $raw_process_info['stop'] === 0 ?
      NULL : $this->dateFormatter->formatTimestamp($raw_process_info['stop']);

    $uptime = $state === ProcessStates::Running ?
      $this->dateFormatter->formatInterval($raw_process_info['now'] - $raw_process_info['start']) : NULL;

    $exit_status = $raw_process_info['exitstatus'] >= 0 && $raw_process_info['stop'] ? $raw_process_info['exitstatus'] : NULL;

    $is_start_allowed = match ($state) {
      ProcessStates::Stopped, ProcessStates::Exited, ProcessStates::Fatal, ProcessStates::Backoff => TRUE,
      default => FALSE,
    };

    $is_stop_allowed = $state === ProcessStates::Running;

    return new Data\ProcessInfo(
      name: $raw_process_info['name'],
      group: $raw_process_info['group'],
      pid: $pid,
      start: $start,
      stop: $stop,
      state: $state,
      stateLabel: self::getStateLabel($state),
      stateColor: self::getStateColor($state),
      uptime: $uptime,
      exitStatus: $exit_status,
      isStartAllowed: $is_start_allowed,
      isStopAllowed: $is_stop_allowed,
      stdoutLogfile: $raw_process_info['stdout_logfile'],
      stderrLogfile: $raw_process_info['stderr_logfile'],
    );
  }

  /**
   * {@selfdoc}
   */
  private static function getStateLabel(ProcessStates $state): string {
    return (string) match ($state) {
      ProcessStates::Stopped => new TM('Stopped'),
      ProcessStates::Starting => new TM('Starting'),
      ProcessStates::Running => new TM('Running'),
      ProcessStates::Backoff => new TM('Backoff'),
      ProcessStates::Stopping => new TM('Stopping'),
      ProcessStates::Exited => new TM('Exited'),
      ProcessStates::Fatal => new TM('Fatal'),
      ProcessStates::Unknown => new TM('Unknown'),
    };
  }

  /**
   * {@selfdoc}
   */
  private static function getStateColor(ProcessStates $state): string {
    return match ($state) {
      ProcessStates::Running => 'green',
      ProcessStates::Starting, ProcessStates::Stopping => 'yellow',
      ProcessStates::Fatal, ProcessStates::Backoff => 'red',
      default => 'gray',
    };
  }

}
