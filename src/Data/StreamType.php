<?php declare(strict_types = 1);

namespace Drupal\supervisor\Data;

/**
 * Types of standard streams.
 */
enum StreamType: string {

  case STDOUT = 'stdout';
  case STDERR = 'stderr';

}
