<?php declare(strict_types = 1);

namespace Drupal\supervisor\Data;

use Supervisor\ProcessStates;

/**
 * A data structure to represent process information.
 */
final readonly class ProcessInfo {

  /**
   * {@selfdoc}
   */
  public function __construct(
    public string $name,
    public string $group,
    public ?int $pid,
    public ?string $start,
    public ?string $stop,
    public ProcessStates $state,
    public string $stateLabel,
    public string $stateColor,
    public ?string $uptime,
    public ?int $exitStatus,
    public bool $isStartAllowed,
    public bool $isStopAllowed,
    public string $stdoutLogfile,
    public string $stderrLogfile,
  ) {}

}
