<?php declare(strict_types = 1);

namespace Drupal\supervisor\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * {@selfdoc}
 */
final class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'supervisor_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['supervisor.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('supervisor.settings');
    $connection_type = $config->get('connection_type');

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => new TM('Connection'),
    ];

    $form['connection']['connection_type'] = [
      '#type' => 'select',
      '#title' => new TM('Type'),
      '#required' => TRUE,
      '#options' => [
        'uds' => new TM('Unix domain socket'),
        'tcp' => new TM('TCP'),
      ],
      '#default_value' => $connection_type,
      '#attributes' => [
        'data-supervisor-selector' => 'connection-type',
      ],
    ];
    $form['connection']['uds_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unix socket path'),
      '#description' => new TM('The path can be absolute or relative to Drupal root directory.'),
      '#default_value' => $config->get('uds_path'),
      '#wrapper_attributes' => [
        'data-supervisor-selector' => 'uds-path',
        'hidden' => $connection_type !== 'uds',
      ],
    ];

    $form['connection']['tcp_url'] = [
      '#type' => 'url',
      '#title' => $this->t('TCP URL'),
      '#default_value' => $config->get('tcp_url'),
      '#wrapper_attributes' => [
        'data-supervisor-selector' => 'tcp-url',
        'hidden' => $connection_type !== 'tcp',
      ],
    ];

    $form['connection']['username'] = [
      '#type' => 'textfield',
      '#title' => new TM('Username'),
      '#disabled' => TRUE,
      '#description' => new TM('Set in your local settings.php file as follows $settings[\'supervisor\'][\'username\'] = \'USERNAME\';'),
    ];

    $form['connection']['password'] = [
      '#type' => 'textfield',
      '#title' => new TM('Password'),
      '#disabled' => TRUE,
      '#description' => new TM('Set in your local settings.php file as follows $settings[\'supervisor\'][\'password\'] = \'PASSWORD\';'),
    ];

    $time = new DrupalDateTime();
    $date_formatter = self::getDateFormatter();
    $options = [];
    foreach (self::getDateFormats() as $type => $type_info) {
      $format = $date_formatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }

    $form['date_format'] = [
      '#type' => 'select',
      '#title' => new TM('Date format'),
      '#description' => new TM('Choose a format for displaying dates.'),
      '#options' => $options,
      '#default_value' => $config->get('date_format'),
    ];

    $form['#attached']['library'][] = 'supervisor/supervisor';
    $form['#attributes']['data-supervisor-selector'] = 'supervisor-settings';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('supervisor.settings')
      ->set('connection_type', $form_state->getValue('connection_type'))
      ->set('uds_path', $form_state->getValue('uds_path'))
      ->set('tcp_url', $form_state->getValue('tcp_url'))
      ->set('date_format', $form_state->getValue('date_format'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@selfdoc}
   */
  private static function getDateFormatter(): DateFormatterInterface {
    return \Drupal::service('date.formatter');
  }

  /**
   * {@selfdoc}
   *
   * @return \Drupal\Core\Datetime\DateFormatInterface[]
   */
  private static function getDateFormats(): array {
    return \Drupal::service('entity_type.manager')->getStorage('date_format')->loadMultiple();
  }

}
